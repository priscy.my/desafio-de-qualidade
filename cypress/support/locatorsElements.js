const locatorsElements = {

    MENU: {
        menuLogin: '.login',
        menuWoman: '.sf-menu > :nth-child(1) > [href="http://automationpractice.com/index.php?id_category=3&controller=category"]',
        menuWomanTops: ':nth-child(1) > h5 > .subcategory-name'
    },

    BANNER:{
        onlyOnline: '#htmlcontent_top > ul > li.htmlcontent-item-2.col-xs-4 > a',
    },

    OPTION:{
        sortByProduct: '#selectProductSort',
    },

    CART: {
        layerCart: '#layer_cart > div.clearfix > div.layer_cart_product.col-xs-12.col-md-6 > div.layer_cart_product_info',
    },

    BUTTON: {
        buttonSubmitLogin: '#SubmitLogin',
        buttonLogout: '.logout',
        buttonAddToCart: '.exclusive > span',
        buttonProceedCheckoutDisplay: '.button-medium > span',
        buttonProceedCheckout: '.cart_navigation > .button > span',   
        buttonAddToCartCatalog: '#center_column > ul > li.ajax_block_product.col-xs-12.col-sm-6.col-md-4.last-line.last-item-of-tablet-line.last-mobile-line > div > div.right-block > div.button-container > a.button.ajax_add_to_cart_button.btn.btn-default',
    },

    LOGIN: {
        fieldLoginEmail: '#email',
        fieldLoginPass: '#passwd',
        welcomeText: '.account',
    },

    ALERT: {
        alertText: 'ol > li',
    },

    PRODUCT: {
        womanBlouse: '.last-item-of-tablet-line > .product-container > .left-block > .product-image-container > .product_img_link > .replace-2x',
        womanBlouseAvailable: '#center_column > ul > li.ajax_block_product.col-xs-12.col-sm-6.col-md-4.last-line.last-item-of-tablet-line.last-mobile-line > div > div.right-block > span',
        womanBlouseReference: '#product_reference > .editable',
        womanBlouseNameList: '.last-item-of-tablet-line > .product-container > .right-block > [itemprop="name"] > .product-name',        
        womanBlousePriceList: '.last-item-of-tablet-line > .product-container > .right-block > .content_price > .price',
        womanBlouseNameDisplay: '[itemprop="name"]',
        womanBlousePriceDisplay: '#our_price_display',

        optionSelect: '#group_1',
        optionSelectSize: '#attributes > fieldset:nth-child(2) > div',
        optionColor: '#color_11',

        layerCartProuctName: '#layer_cart_product_title',
        layerCartProductPrice: '#layer_cart_product_price',

        sumaryCartAvailable: '.label',

        //ADDRESS
        addressEqualsCheck: '#addressesAreEquals',

        processHeaderTitle: '.page-heading',

        //SHIPPING
        shipTerms: '#cgv',
        shipCarrier: '#delivery_option_555661_0',

        msgBoxError: '#order > div.fancybox-overlay.fancybox-overlay-fixed > div > div > div',
        msgBoxErrorClose: '#order > div.fancybox-overlay.fancybox-overlay-fixed > div > div > a',

        //PAYMENT
        payUnitProductPrice: '#product_price_2_9_555661 > .price',
        payQntProduct: '.cart_quantity > span',
        payShipping: '#total_shipping',
        payTotal: '#total_price',
        payBankwire: '#HOOK_PAYMENT > div:nth-child(1) > div > p > a',
        payTotalConfirm: '#amount',

        buttonBackOrders: '.button-exclusive',
        
        historyOrderList: '#order-list > tbody > tr.first_item'
    }

}

export default locatorsElements;