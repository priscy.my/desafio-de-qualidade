/// <reference types= "cypress" />

import loc from '../support/locatorsElements'

describe('Fluxo de Compras', () => {
    before(() =>{
        cy.visit({url:'http://automationpractice.com/index.php', failOnStatusCode: false})
    })

    it('Realizando compra de blusa feminina com sucesso', function(){
        //LOGIN
        cy.get(loc.MENU.menuLogin).click();
        cy.fixture('dataUser').as('user').then(() => {
            cy.get(loc.LOGIN.fieldLoginEmail)
                .type(this.user.email)
                .should('not.be.null')
            cy.get(loc.LOGIN.fieldLoginPass)
                .type(this.user.password)
                .should('not.be.null')
            cy.get(loc.BUTTON.buttonSubmitLogin).click()
                cy.get(loc.LOGIN.welcomeText)
                .should('contain',this.user.name)
        })

        cy.get(loc.MENU.menuWoman).click()
        cy.get(loc.MENU.menuWomanTops, {timeout: 15000}).click()


        let descProduto = '';
        cy.get(loc.PRODUCT.womanBlouseNameList).invoke('text').then($value => {
            descProduto = $value.trim();
        });

        let blousePrice = '';
        cy.get(loc.PRODUCT.womanBlousePriceList).invoke('text').then($value => {
            blousePrice = $value.trim();
        });
        
        cy.get(loc.PRODUCT.womanBlouseAvailable).should('contain', 'In stock')
        cy.get(loc.PRODUCT.womanBlouse).click()

        cy.get(loc.PRODUCT.womanBlouseNameDisplay)
            .should('be.visible').and('contain', 'Blouse')

        cy.get(loc.PRODUCT.optionSelect)
            .select('M')    
        cy.get(loc.PRODUCT.optionColor).click()
        cy.get(loc.BUTTON.buttonAddToCart).click()

        cy.get(loc.PRODUCT.womanBlouseNameDisplay).invoke('text').then(($value_2) => { 
            expect(descProduto).to.eq($value_2)
        })

        cy.get(loc.PRODUCT.womanBlousePriceDisplay).invoke('text').then(($value_2) => { 
            expect(blousePrice).to.eq($value_2)
        })
        cy.get(loc.BUTTON.buttonProceedCheckoutDisplay, {timeout:10000}).click()

        //SUMARY
        cy.get(loc.PRODUCT.sumaryCartAvailable).should('contain', 'In stock')

        //ADDRESS
        cy.get(loc.BUTTON.buttonProceedCheckout).click()
        cy.get(loc.PRODUCT.addressEqualsCheck).should('be.checked')
        cy.get(loc.BUTTON.buttonProceedCheckout).click()

        //SHIPPING
        cy.get(loc.BUTTON.buttonProceedCheckout).click()
        cy.get(loc.PRODUCT.msgBoxError)
            .should('be.visible')
            .and('contain', 'agree to the terms')
        cy.get(loc.PRODUCT.msgBoxErrorClose).click()

        cy.get(loc.PRODUCT.shipTerms).click().should('be.checked')
        cy.get(loc.BUTTON.buttonProceedCheckout).click()

        //PAYMENT
        cy.get(loc.PRODUCT.payBankwire).click()
        cy.get(loc.BUTTON.buttonProceedCheckout).click()
        
        //ORDERS
        cy.get(loc.PRODUCT.buttonBackOrders).click()
        cy.get(loc.PRODUCT.historyOrderList, {timeout:10000}).click()
        cy.get('.footable-row-detail-value > .btn > span').click()

        cy.get('.last_item > :nth-child(2) > .label')
            .should('exist')
            .and('contain', 'bank wire')
    })
    
})



