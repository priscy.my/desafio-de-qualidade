/// <reference types= "cypress" />

import loc from '../support/locatorsElements'

describe('Fluxos de Compras SEM SUCESSO', () => {
    before(() =>{
        cy.visit({url:'http://automationpractice.com/index.php', failOnStatusCode: false})
    })

    it('Realizando compra de produtos na sessão ONLY ONLINE do site', () => {
        cy.get(loc.BANNER.onlyOnline).click()
        cy.get(loc.OPTION.sortByProduct).should('be.visible')

        })

    it('Adicionando o produto direto no carrinho de compras', () => {
        cy.get(loc.MENU.menuWoman).click()
        cy.get(loc.MENU.menuWomanTops, {timeout: 10000}).click()
        
        cy.get(loc.BUTTON.buttonAddToCartCatalog).click()

        cy.get(loc.CART.layerCart)
            .should('contain', 'Blouse')
            .and('contain', 'Size')
            .and('contain', 'Color')
            .and('contain', 'Quantity')
        cy.get(loc.PRODUCT.optionSelectSize).should('exist')
    
        })

})