/// <reference types= "cypress" />

import loc from '../support/locatorsElements'

describe('Login', () => {
    before(() =>{
        cy.visit({url:'http://automationpractice.com/index.php', failOnStatusCode: false})
    })

    it('Realizando login válido', function(){
        cy.get(loc.MENU.menuLogin).click();
        cy.fixture('dataUser').as('user').then(() => {
            cy.get(loc.LOGIN.fieldLoginEmail)
                .type(this.user.email)
                .should('not.be.null')
            cy.get(loc.LOGIN.fieldLoginPass)
                .type(this.user.password)
                .should('not.be.null')

            cy.get(loc.BUTTON.buttonSubmitLogin).click()
            cy.get(loc.LOGIN.welcomeText)
                .should('contain',this.user.name)
        })  
            cy.get(loc.BUTTON.buttonLogout).click();

    })
    
    it('Realizando login sem preeencher credenciais', ()=>{
        cy.get(loc.LOGIN.fieldLoginEmail).should('not.have.value')
        cy.get(loc.LOGIN.fieldLoginPass).should('not.have.value')

        cy.get(loc.BUTTON.buttonSubmitLogin).click()
        cy.alertMessage(loc.ALERT.alertText, 'required')
        
    })

    it('Realizando login sem cadastro', ()=>{
        cy.get(loc.LOGIN.fieldLoginEmail)
            .type("naosxite@gmail.com")
            .should('not.be.null')

        cy.get(loc.LOGIN.fieldLoginPass)
            .type("123456")
            .should('not.be.null')

        cy.get(loc.BUTTON.buttonSubmitLogin).click()
        cy.alertMessage(loc.ALERT.alertText, 'failed')

    })

})