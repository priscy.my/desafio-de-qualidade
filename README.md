DESAFIO QUALIDADE
AUTOMAÇÃO: http://automationpractice.com/index.php

NO TERMINAL:
npm install cypress

COMANDO PARA INICIAR O CYPRESS:
./node_modules/.bin/cypress open

cypress/fixtures/dataUser.json
dados de credenciais utilizados para asserção de login válido

cypress/support/commands.js
comandos criados para utilização

cypress/support/locatorsElements.js
seletores dos elementos utilizados para asserções dos testes

cypress/integration/login.spec.js
Cenários de testes simulando login válido, login sem credenciais e login sem usuário cadastrado

cypress/integration/shoppingFlow.spec.js
Cenários de testes com o fluxo de compras efetuado com sucesso

cypress/integration/shoppingFail.spec.js
Cenários de testes com fluxos de compras sem sucesso.
- Para o primeiro cenário de Teste, era esperado que ao clicar no banner "ONLY ONLYNE", o usuário fosse redirecionado para a página com Listagem de produtos.
- Para o segundo cenário, era esperado que ao clicar em "ADD TO CART" de um produto, o usuário tivesse o acesso à escolha das opções daquele determinado produto - tais como: cor, quantidade, tamanho - o que permite que a compra seja efetuada sem o usuário selecionar as variações.

O critério de escolha de testes foi baseada em testes funcionais e levando em consideração algumas funcionalidades críticas, por exemplo:
- a validação de aceitação dos termos de entrega,
- a validação do login
- a validação se o valor do produto corresponde em duas das etapas do fluxo da compra